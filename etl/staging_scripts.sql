COPY staging.legacy_facts_billitem FROM 's3://ca.tb.dw.dev/facts_billitem/'
credentials 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user'
compupdate off 
statupdate off
IGNOREHEADER AS 1
CSV QUOTE AS '`' 
region 'ca-central-1'
;

COPY staging.legacy_facts_billitem FROM 's3://ca.tb.dw.dev/facts_billitem/'
credentials 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user'
compupdate off 
statupdate off
IGNOREHEADER AS 1
CSV
region 'ca-central-1'
;

---------------------------------------------------------------------------------------

COPY staging.legacy_facts_bill FROM 's3://ca.tb.dw.dev/facts_bills/2019/09/09/13/30'
credentials 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user'
compupdate off 
statupdate off
IGNOREHEADER AS 1
CSV QUOTE AS '`' 
region 'ca-central-1'
;

---------------------------------------------------------------------------------------

COPY staging.legacy_facts_discountitem FROM 's3://ca.tb.dw.dev/facts_discountitem/'
credentials 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user'
compupdate off 
statupdate off
IGNOREHEADER AS 1
CSV QUOTE AS '`' 
region 'ca-central-1'
;

---------------------------------------------------------------------------------------

COPY staging.legacy_facts_order_payment FROM 's3://ca.tb.dw.dev/facts_order_payment/'
credentials 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user'
compupdate off 
statupdate off
IGNOREHEADER AS 1
CSV QUOTE AS '`' 
region 'ca-central-1'
;

---------------------------------------------------------------------------------------

COPY staging.legacy_facts_staff_shift FROM 's3://ca.tb.dw.dev/facts_staff_shift/'
credentials 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user'
compupdate off 
statupdate off
IGNOREHEADER AS 1
CSV QUOTE AS '`' 
region 'ca-central-1'
;

