# TB Pipeline Airflow

this project is based on the excellent [puckel/airflow](https://github.com/puckel/docker-airflow) github repository. Please see the Original README file in this dirctory for details on the design and structure of the containers.

When the container is built for the first time, a user will be created called 'admin' with the password 'changeme_99'
If this is a local system you can keep it as-is. For production systems this user should be deactivated once named users are created. It should not be deleted as it will get re-created on next container startup.

# DEV Workflow
TODO: fill this in better
* 

## Building the container
docker build -t tb/airflow .

## Running the container locally for testing and dev
docker run -d -p 8080:8080 tb/airflow webserver
## same thing - don't supress output
docker run -p 8080:8080 tb/airflow webserver

## looking at running containers
docker container ls

## Stoping a running container
docker container stop [container name]

## copy files from local to container if you don't want to rebuild
docker cp dags/ [containerid]:/usr/local/airflow/


## for debugging, open bash prompt in container
docker exec -it <container name> /bin/bash


## Pushing container to AWS
aws ecr get-login --no-include-email --region ca-central-1 

  cut and paste the result of the above into the console before calling the following commands

## This repo path is for dev!
set REPO_PATH=281621781242.dkr.ecr.ca-central-1.amazonaws.com/tb/airflow
set TAG=20190611c
# Use this command to push to repo - set your parameter above
docker tag tb/airflow "%REPO_PATH%:%TAG%"
docker push "%REPO_PATH%:%TAG%"