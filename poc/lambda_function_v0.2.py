import helpers

def lambda_handler(event, context):
    
    source_bucket = helpers.set_source_bucket()

    #filepath = get_file_path('bills', str(event['time']))
    filepath = f's3://{source_bucket}/facts_billitem/venue/'
    key = filepath.split(f'{source_bucket}/')[1]

    helpers.s3_check_object(source_bucket, key)

    failure_handle_statement = """SELECT filename FROM stl_load_errors WHERE filename like '%facts_billitem%' OrDER BY starttime desc limit 1;"""
    
    copy_statement = (""" COPY staging.legacy_facts_billitem FROM '{}'
                        CREDENTIALS '{}'
                        COMPUPDATE OFF 
                        IGNOREHEADER AS 1
                        DELIMITER ','
                        REGION 'ca-central-1'
                        """).format(filepath, 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user')
    
    helpers.execute_query(copy_statement, failure_handle_statement)