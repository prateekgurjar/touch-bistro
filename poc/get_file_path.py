import datetime

def get_file_path(filename, time):

	time_ts = datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
	previous_5_min = time_ts - datetime.timedelta(seconds=300)
	logical_partition = datetime.datetime.strftime(previous_5_min, "%Y/%m/%d/%H/%M")
	
	file_path = f's3://ca.tb.dw.dev/{filename}/{logical_partition}'

	return file_path

if __name__ == '__main__':
	
	filename = 'parts'
	time = '2019-08-21T20:07:00Z'
	file_path = get_file_path(filename, time)
	print(file_path)
