import json
import configparser
import psycopg2
import boto3 
import datetime
import boto3
import sys

def set_source_bucket():
    source_bucket = 'ca.tb.dw.dev'
    return source_bucket

def set_failed_object_bucket():
    failed_objects_bucket = 'ca.tb.dw.failed'
    return failed_objects_bucket

def get_file_path(table_name, time):

    time_ts = datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
    previous_5_min = time_ts - datetime.timedelta(seconds=300)
    previous_5_min = previous_5_min - datetime.timedelta(hours=4)
    logical_partition = datetime.datetime.strftime(previous_5_min, "%Y/%m/%d/%H/%M")
    file_path = f's3://{set_source_bucket()}/{table_name}/{logical_partition}'
    return file_path

def create_connection(config_file):

    config = configparser.ConfigParser()
    config.read(config_file)
    conn_string = "host={} dbname={} user={} password={} port={}".format(*config['CLUSTER'].values())
    conn = psycopg2.connect(conn_string)
    return conn

def s3_copy_failed_objects(source_bucket, failed_objects_bucket, failed_file_name):
    
    s3 = boto3.resource('s3')
    key = failed_file_name
    print(f'Starting process to copy {failed_file_name} from {source_bucket} to {failed_objects_bucket}')
    
    try:
        copy_source = {'Bucket': source_bucket,'Key': key}
        s3.meta.client.copy(copy_source, failed_objects_bucket, key)
        print(f'{key} copied to {failed_objects_bucket} bucket')
    except Exception as e:
        print(e)

def s3_delete_failed_object(bucket_name, failed_file_name):
    ## Write logic
    s3 = boto3.resource('s3')
    s3.Object(bucket_name, failed_file_name).delete()

def s3_check_object(bucket_name, key):
    ## Write logic to confirm if the S3 Object exists
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    obj = list(bucket.objects.filter(Prefix=key))
    if len(obj) > 0:
        return 1
    else:
        return 0

def execute_query(copy_statement, failure_handle_statement):
    
    config_file = 'dwh.cfg'
    source_bucket = set_source_bucket()
    conn = create_connection(config_file)
    cur = conn.cursor()
    success_flag = 0
    retry_attempt = 0
    
    try:
        cur.execute(copy_statement)
        conn.commit()
        print('query exec success')
        success_flag = 1
    except Exception as e:
        print(e)
        while retry_attempt < 3:
            print(f'Trying attempt {retry_attempt}')
            retry_attempt+= 1
            conn.rollback()
            cur.execute(failure_handle_statement)
            failed_file_obj = cur.fetchone()
            file_name = failed_file_obj[0].split(f's3://{set_source_bucket()}/')[1].strip()
            conn.commit()
            s3_copy_failed_objects(source_bucket, set_failed_object_bucket(), file_name)
            s3_delete_failed_object(source_bucket, file_name)
            try:
                cur.execute(copy_statement)
                conn.commit()
                print('query exec success')
                success_flag = 1
                break
            except Exception as e:
                print(e)
                conn.rollback()
        
    if success_flag == 1:
        if retry_attempt == 0:
            print('Copy objects successful')
        else:
            print(f'Copy successful after {retry_attempt} attempts')
            print(f'Error files placed in {set_failed_object_bucket()}. Please fix and rerun the Failed objects lambda function')
    else:
        print('More than 3 csv files corrupted.')

    conn.close()