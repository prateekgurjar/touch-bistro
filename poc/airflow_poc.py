import boto3
import psycopg2
import configparser

s3 = boto3.resource('s3', aws_access_key_id='AKIAYW57NVD4M7MEP5NB', aws_secret_access_key= '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')
bucket_name = 'ca.tb.dw.dev'
key = 'part'
bucket_object = s3.Object(bucket_name, key)

bucket = s3.Bucket(bucket_name)
config = configparser.ConfigParser()
config.read('dwh.cfg')

file_list = []
for my_bucket_object in bucket.objects.filter(Prefix='part/'):
    file = my_bucket_object.key.split('/')[1]
    if file == '' or 'batched' in file:
        pass
    else:
        file_list.append(file)

conn_string = "host={} dbname={} user={} password={} port={}".format(*config['CLUSTER'].values())
conn = psycopg2.connect(conn_string)
cur = conn.cursor()

for file in file_list:
    
    s3_file_path = 's3://'+bucket_name+'/'+key+'/'+file   
    new_file_path = key+'/'+'batched_'+file
    
    try:

        query = (""" COPY staging.part FROM '{}'
                     credentials '{}'
                     compupdate off 
                     csv
                     region 'ca-central-1'
                     null as '\\000'
                     """).format(s3_file_path, 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user')
        
        cur.execute(query)             
        conn.commit()
        s3.Object('ca.tb.dw.processed', new_file_path).copy_from(CopySource=bucket_name+'/'+key+'/'+file)    
        s3.Object(bucket_name, key+'/'+file).delete()

    except Exception as error:
        
        s3.Object('ca.tb.dw.failed', new_file_path).copy_from(CopySource=bucket_name+'/'+key+'/'+file)
        s3.Object(bucket_name, key+'/'+file).delete()
        print(error)

conn.close()