import json
import configparser
import psycopg2
import boto3 

def lambda_handler():
    # TODO implement
    ## s3 = boto3.client("s3", "AKIAYW57NVD4M7MEP5NB", "wjDD0dYuQXANYSeC3+SVVvvHtYNGTecjUvaJr88m")
    s3 = boto3.client("s3")
    filename = 's3://ca.tb.dw.dev/part/part-csv.tbl-000'
    print("testing config")

    config = configparser.ConfigParser()
    config.read('dwh.cfg')
    
    print("test config passed")

    query = (""" COPY staging.part FROM '{}'
                 credentials '{}'
                 compupdate off 
                 csv
                 region 'ca-central-1'
                 null as '\\000'
                 """).format(filename, 'aws_iam_role=arn:aws:iam::599012518136:role/redshift_lambda_user')
    
    conn_string = "host={} dbname={} user={} password={} port={}".format(*config['CLUSTER'].values())
    
    print(conn_string)
    
    conn = psycopg2.connect(conn_string)
    print('conn success')
    cur = conn.cursor()
    print('cur success')
    cur.execute(query)
    print('query exec success')
    conn.commit()
    print('conn commit success')

if __name__ == '__main__':
    lambda_handler()