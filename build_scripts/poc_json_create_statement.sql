DROP TABLE staging.poc_json;

create table staging.poc_json
(
  batch_id int
, batch_timestamp timestamp
, facts_order_payment varchar(max)
, facts_staff_shift varchar(max)
, facts_bill_items varchar(max)
, facts_discount_items varchar(max)
, facts_bills struct
);

drop table staging.poc_json2;
create table staging.poc_json2
(
  load_timestamp_utc timestamp
, load_batch_uuid text
, bills varchar(max)
, billitems varchar(max)
, discountitems varchar(max)
, order_payments varchar(max)
, staff_shifts varchar(max)
);

SELECT 
  load_batch_uuid
, bills
, load_timestamp_utc
FROM staging.poc_json2

select * from pg_group
